const API_VERSION 	= 'v1.0';
const PROJECT_TITLE	= 'TechU 2018';
const PROJECT_NAME	= 'Bank-API';

//    Package's & Moduls Dependencies        
const express       = require('express'); 
const router        = express.Router(); 
const bodyParser    = require('body-parser'); 
const jsonQuery     = require('json-query'); 
const stringify     = require('json-stringify'); 
const requestJson   = require('request-json'); 
//const log           = require('log4js').getLogger("bank");
const path          = require('path');

//    Config Variables
var  mlab_base_path 	= "https://api.mlab.com/api/1/databases/mx-demo-techui-fm/collections"
var  mlab_app_key   	= "apiKey=7DiYqBGNHsLCbWveVW9qAjQ0ATuXQ2VE"
var  mlab_client		=  requestJson.createClient( mlab_base_path + "?" + mlab_app_key )


//____________________________________________USUARIOS
router.get('/usuarios', function(req, res){
	console.log("Bank-API :: /usuarios ");
	var query_filter  	= '?c=true&'
	var maxUserId;
	mlab_client			=  requestJson.createClient( mlab_base_path + "/usuarios?"  );
	mlab_client.get(query_filter+mlab_app_key , function(_error, _resM, _body ){
		maxUserId  		= parseInt(_body);
		//var usuarios 	= [];
		//usuarios.push ( {"total_usuarios": maxUserId } )
		res.json( {"total_usuarios": maxUserId });
	//	usuarios.push ( {"data":_body} )
	//	res.json( {"total_usuarios": maxUserId } )
	})
})



// usuario acceso / "email":"rmalarkey2l@wisc.edu","password":"12345" post
router.post('/usuarios/acceso', function(req, res){
// 	VALIDA REQUERIDOS:    nombre, apellido, email, pais, password
//	console.log( "Data : ", user)
	var query_filter  	= '';
	var errors;
	req.checkBody("email", "El 'email' no es correcto, favor de verificar.").isEmail();
	req.checkBody("password", "El 'password' no cumple con longitud minima de 5, favor de verificar.").exists().isLength({ min: 5 });
	errors 				= req.validationErrors();				
	if (errors) {
		res.status(406).send(errors);	//	Status = 406 - Not Acceptable  https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		return;
	} else {  // ERR REQ
		var acc_email 			= req.body.email
		var acc_passw 			= req.body.password
		console.log("Bank-API :: /usuarios/acceso  :: email ::["+acc_email+"] "  );		
	//	Validation :: EXIST EMAIL & PASSWORD 
		mlab_client		= requestJson.createClient( mlab_base_path + "/usuarios" );
		query_filter  	= '?q={"email":"'+acc_email+'","password":'+acc_passw+'}&';
		mlab_client.get( query_filter + mlab_app_key , function(error, res_1 , body){
			var dat_user = body
		//	console.log("Bank-API :: /usuarios/acceso  dat_user::[ "+dat_user+" ] "  );
		//	console.log("Bank-API :: /usuarios/acceso  MLAB body.length:[ "+ body.length+" ]     " );
			existEmail 	= parseInt(body.length);
			if(existEmail<=0){
			//	Status = 406 - Not Acceptable  https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html				
				res.status(406).send("La conbinacion de usuario y password NO es correcta, verifique sus datos. ");
				return;
			}else{
			//	console.log("Bank-API :: /usuarios/acceso  ID::["+body.id+"] idusuario::["+body.idusuario+"] "  );
				var user   	= []
				res.send({'data':body});
				return;
			}// ELSE ID USER	
		})// EXIST EMAIL 
	}// ELSE ERR REQ
})



// usuario post
router.post('/usuarios', function(req, res){
// 	VALIDA REQUERIDOS:    nombre, apellido, email, pais, password
//	console.log( "Data : ", new_user)
	var new_user 		= req.body
	var query_filter  	= '';
	var errors;
	req.checkBody("nombre", "El 'nombre' no es correcto, favor de verificar.").isAlpha('es-ES');
	req.checkBody("apellido", "El 'apellido' no es correcto, favor de verificar.").isAlpha('es-ES');
	req.checkBody("pais", "El 'pais' no es correcto, favor de verificar.").exists();
	req.checkBody("password", "El 'password' no cumple con longitud minima de 6, favor de verificar.").exists().isLength({ min: 6 });
	req.checkBody("email", "El 'email' no es correcto, favor de verificar.").isEmail();
	errors 			= req.validationErrors();
	if (errors) {
		res.status(406).send(errors);	//	Status = 406 - Not Acceptable  https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		return;
	} else {  // ERR REQ
	//	Before Insert :: EXIST EMAIL 
		mlab_client		= requestJson.createClient( mlab_base_path + "/usuarios" );
		query_filter  	= '?q={"email":"'+new_user.email+'"}&c=true&';
		mlab_client.get( query_filter + mlab_app_key , function(error, res_1 , body){
			existEmail 	= parseInt(body);
			console.log("Bank-API :: /usuarios  Valid existEmail:["+existEmail+"] T["+(existEmail>0)+"] email[" +new_user.email+ "]"  );
			if(existEmail>0){				
			//	Status = 406 - Not Acceptable  https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
				console.log("Bank-API :: /usuarios  email:[" +new_user.email+ "] ya existe !"  );
				res.status(406).send("El 'email' ya está registrado. ");
				return;
			}else{
			//	Before Insert :: MAX ID USER	
				query_filter  		= '?c=true&'
				mlab_client.get( query_filter + mlab_app_key , function(error, res_2 , body_2){
					maxUserId  	= parseInt(body_2);
					nextUserId 	= (maxUserId+1)
					var d = new Date();
					var d_iso_8601 = d.toISOString();  // ISO 8601 
				//	DEFAULT VALUES	 
					new_user.estado  	= "fueraLinea"			
					new_user.idusuario 	= "USR" +  nextUserId
					new_user.id  		= nextUserId
					new_user.accesos   	= {"registro":d_iso_8601 }					
					console.log("Bank-API :: /usuarios  maxUserId:["+maxUserId+"] New User Next ID:["+nextUserId+"] email:[" +new_user.email+ "]");
					query_filter  		= '?';
					mlab_client.post( query_filter + mlab_app_key , req.body , function(error, res_3 , body_3){						
						var new_user   	= []
						    new_user.push({'SUCCESS':body_3});
						res.send( new_user )
					})
				})
			}// ELSE MAX ID USER	
		})// EXIST EMAIL 
	}// ELSE ERR REQ
})

// usuarios 
router.post('/usuarios/:id', function(req, res){
	var query_filter  	= '';
	console.log("Bank-API :: /usuarios  id:[" +req.params.id+ "]");
	
	req.checkParams("id", "El 'id' usuarios no es correcto").notEmpty().contains('USR');
	
	var errors = req.validationErrors();
	if (errors) {
	//	Status = 406 - Not Acceptable  https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
	//	console.log("Bank-API :: /usuarios  id:[" +req.params.id+ "]");
		mlab_client		= requestJson.createClient( mlab_base_path + "/usuarios" )
		var query  		= '?q={"idusuario":'+req.params.id+'}&'
		mlab_client.get( query_filter + mlab_app_key , function(error, res_1 , body){
			var dat_user = body
			console.log("Bank-API :: /usuarios/id  dat_user::[ "+dat_user+" ] "  );
		//	console.log("Bank-API :: /usuarios/acceso  MLAB body.length:[ "+ body.length+" ]     " );
			existEmail 	= parseInt(body.length);
			if(existEmail<=0){
			//	Status = 406 - Not Acceptable  https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html				
				res.status(406).send("El usuario ID NO es correcta, verifique sus datos. ");
				return;
			}else{
				console.log("Bank-API :: /usuarios/acceso  ID::["+body.id+"] idusuario::["+body.idusuario+"] "  );
				var user   	= []
				res.send({'data':body});
				return;
			}// ELSE ID USER	
		})// EXIST EMAIL 
	}
})

// usuarios 
router.put('/usuarios/:id', function(req, res){
	console.log("Bank-API :: /usuarios  id:[" +req.params.id+ "]");
	
	req.checkParams("id", "El 'id' usuarios no es correcto").notEmpty().contains('USR');
	
	var errors = req.validationErrors();
	if (errors) {
	//	Status = 406 - Not Acceptable  https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
		console.log("Bank-API :: /usuarios  id:[" +req.params.id+ "]");
		mlab_client		= requestJson.createClient( mlab_base_path + "/usuarios" )
		var query  		= '?q={"idusuario":'+req.params.id+'}&'
		var cambios 	= '&{ "$set":{'+stringify(req.body)+'}}'
		console.log ("cambios: Y	"  +cambios)
		mlab_client.put( query + mlab_app_key, function(error, _resM , cambios){
			res.send( cambios )
		})
	}
})




module.exports = router;