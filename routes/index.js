const API_VERSION 	= 'v1.0';
const PROJECT_TITLE	= 'TechU 2018';
const PROJECT_NAME	= 'Bank-API';

//    Package's & Moduls Dependencies		
const express 		= require('express');
const router 		= express.Router();
//const log 			= require('log4js').getLogger("index");
const requestJson	= require('request-json');
const bodyParser 	= require('body-parser');
const jsonQuery 	= require('json-query');
const stringify 	= require('json-stringify');
const path 			= require('path')


// GET home page  --  API Definition's 
router.get('/', (req, res) => {
	console.log("Home Bank-API Module");
	var info = '<div id="ascii-gallery" class="ascii-default-style"></div><script type="text/javascript">'
		info = info  + `var ascii_text="${PROJECT_TITLE}  /  ${PROJECT_NAME} ";var ascii_style="big";`
		info = info  + '</script><script type="text/javascript" src="https://textart.io/gadgets/ascii.figlet.js"></script>';
	res.end( info , { title: 'Express' });
});

module.exports = router;