const API_VERSION 	= 'v1.0';
const PROJECT_TITLE	= 'TechU 2018';
const PROJECT_NAME	= 'Bank-API';

//    Package's & Moduls Dependencies        
const express       = require('express'); 
const router        = express.Router(); 
const bodyParser    = require('body-parser'); 
const jsonQuery     = require('json-query'); 
const stringify     = require('json-stringify'); 
const requestJson   = require('request-json'); 
//const log           = require('log4js').getLogger("bank");
const path          = require('path');

//    Config Variables
var  mlab_base_path 	= "https://api.mlab.com/api/1/databases/mx-demo-techui-fm/collections"
var  mlab_app_key   	= "apiKey=7DiYqBGNHsLCbWveVW9qAjQ0ATuXQ2VE"
var  mlab_client		=  requestJson.createClient( mlab_base_path + "?" + mlab_app_key )

// ____________________________________________CUENTAS
router.get('/cuentas', function(req, res){
	console.log("Bank-API :: /cuentas ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
	var query = '?f={"movimientos":0}&' + mlab_app_key
	mlab_client.get( query, function(error, _resM , _body){
		var cuentas = [];		
		cuentas.push ( {"total_cuentas":_body.length} )
	//	cuentas.push ( {"data":_body} )
		res.send( cuentas )
	})
})

router.post('/cuentas/:id', function(req, res){	
	console.log("Bank-API :: /cuentas  id:[" +req.params.id+ "]");

	req.checkParams("id", "El 'id' cuentas no es correcto").notEmpty().contains('CTA');
	
	var errors = req.validationErrors();
	if (errors) {
	//	Status Code Definitions / 406 Not Acceptable  ## https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
		mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
		var query = '?q={"id":'+req.params.id+'}&f={"movimientos":0}&' + mlab_app_key
	//	console.log (query)
		mlab_client.get( query, function(error, _resM , body){
			console.log("Bank-API :: /cuentas/:id  ID::["+body.id+"] idcuenta::["+body.idcuenta+"] idusuario::["+body.idusuario+"] "  );
			res.send( body )
		})
	}
})

router.post('/cuentas/:id/movimientos', function(req, res){
	console.log("Bank-API :: /cuentas  id:[" +req.params.id+ "] /movimientos ");	

	var errors = req.validationErrors();
	if (errors) {
	//	Status Code Definitions / 406 Not Acceptable  ## https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
		mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
		var query = '?q={"id":'+req.params.id+'}&f={"movimientos":1}&' + mlab_app_key
	//	console.log (query)
		mlab_client.get( query, function(error, _resM , _body){
			res.send( _body )
		})
	}
})


router.put('/cuentas/:id', function(req, res){
	console.log("Bank-API :: /cuentas  id:[" +req.params.id+ "] ");
	mlab_client		= requestJson.createClient( mlab_base_path + "/cuentas" )
	var query  		= '?q={"id":'+req.params.id+'}&'
	var cambios 	= '&{ "$set":{'+stringify(req.body)+'}}'
//	console.log ("cambios: Y	"  +cambios)
	mlab_client.put( query + mlab_app_key, function(error, _resM , cambios){
		res.send( cambios )
	})
})
// ____________________________________________MOVIMIENTOS

router.get('/movimientos', function(req, res){
	console.log("Bank-API :: /movimientos ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/movimientos?" + mlab_app_key )
	mlab_client.get( '' , function(_error, _resM, _body ){
		var movimientos = [];		
		movimientos.push ( {"total_movimientos":_body.length} )
	//	movimientos.push ( {"data":_body} )
		res.send( movimientos )
	})
})

router.post('/movimientos', function(req, res){
	console.log("Bank-API :: /movimientos ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/movimientos?" + mlab_app_key )
	mlab_client.post( '' , req.body , function(error, _resM , _body){
		res.send( _body )
	})
})

router.post('/movimientos/:id', function(req, res){	
	console.log("Bank-API :: /movimientos  id:[" +req.params.id+ "] ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/movimientos" )
	var query = '?q={"idmovimiento":'+req.params.id+'}&'
//	console.log (query)
	mlab_client.get( query + mlab_app_key, function(error, _resM , _body){
		res.send( _body )
	})
})
router.post('/movimientos/cuentas/:id', function(req, res){	
	console.log("Bank-API :: /movimientos/cuentas/  id:[" +req.params.id+ "] ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
	var query = '?q={"id":'+req.params.id+'}&f={"movimientos":1}&' + mlab_app_key
//	console.log (query)
	mlab_client.get( query, function(error, _resM , _body){
		console.log("Bank-API :: /movimientos/cuentas/:id "  + _body   );
		res.send( _body )
	})
})

router.put('/movimientos/:id', function(req, res){	
	console.log("Bank-API :: /movimientos  id:[" +req.params.id+ "] ");
	mlab_client		= requestJson.createClient( mlab_base_path + "/movimientos" )
	var query  		= '?q={"idmovimiento":'+req.params.id+'}&'
	var cambios 	= '&{ "$set":{'+stringify(req.body)+'}}'
//	console.log ("cambios: Y	"  +cambios)
	mlab_client.put( query + mlab_app_key, function(error, _resM , cambios){
		res.send( cambios )
	})
})



/*


// ____________________________________________CUENTAS
router.get('/cuentas', function(req, res){
	console.log("Bank-API :: /cuentas ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
	var query = '?f={"movimientos":0}&' + mlab_app_key
	mlab_client.get( query, function(error, _resM , _body){
		var cuentas = [];		
		cuentas.push ( {"total_cuentas":_body.length} )
		cuentas.push ( {"data":_body} )
		res.send( cuentas )
	})
})

router.post('/cuentas/:id', function(req, res){	
	console.log("Bank-API :: /cuentas  id:[" +req.params.id+ "]");

	req.checkParams("id", "El 'id' cuentas no es correcto").notEmpty().contains('CTA');
	
	var errors = req.validationErrors();
	if (errors) {
	//	Status Code Definitions / 406 Not Acceptable  ## https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
		mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
		var query = '?q={"idcuenta":'+req.params.id+'}&f={"movimientos":0}&' + mlab_app_key
	//	console.log (query)
		mlab_client.get( query, function(error, _resM , _body){
			res.send( _body )
		})
	}
})

router.post('/cuentas/:id/movimientos', function(req, res){
	console.log("Bank-API :: /cuentas  id:[" +req.params.id+ "] /movimientos ");	

	var errors = req.validationErrors();
	if (errors) {
	//	Status Code Definitions / 406 Not Acceptable  ## https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
		mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
		var query = '?q={"idcuenta":'+req.params.id+'}&f={"movimientos":1}&' + mlab_app_key
	//	console.log (query)
		mlab_client.get( query, function(error, _resM , _body){
			res.send( _body )
		})
	}
})


router.put('/cuentas/:id', function(req, res){
	console.log("Bank-API :: /cuentas  id:[" +req.params.id+ "] ");
	mlab_client		= requestJson.createClient( mlab_base_path + "/cuentas" )
	var query  		= '?q={"idcuenta":'+req.params.id+'}&'
	var cambios 	= '&{ "$set":{'+stringify(req.body)+'}}'
//	console.log ("cambios: Y	"  +cambios)
	mlab_client.put( query + mlab_app_key, function(error, _resM , cambios){
		res.send( cambios )
	})
})


*/


module.exports = router;




