const API_VERSION 	= 'v1.0';
const PROJECT_TITLE	= 'TechU 2018';
const PROJECT_NAME	= 'Bank-API';

//    Package's & Moduls Dependencies        
const express       = require('express'); 
const router        = express.Router(); 
const bodyParser    = require('body-parser'); 
const jsonQuery     = require('json-query'); 
const stringify     = require('json-stringify'); 
const requestJson   = require('request-json'); 
//const log           = require('log4js').getLogger("bank");
const path          = require('path');

//    Config Variables
var  mlab_base_path 	= "https://api.mlab.com/api/1/databases/mx-demo-techui-fm/collections"
var  mlab_app_key   	= "apiKey=7DiYqBGNHsLCbWveVW9qAjQ0ATuXQ2VE"
var  mlab_client		=  requestJson.createClient( mlab_base_path + "?" + mlab_app_key )
//___________________________________________ Resources 
router.get('/', function(req, res){
    console.log("Bank-API :: Resources :: Module");
	mlab_client		=  requestJson.createClient( mlab_base_path + "?" + mlab_app_key )
	mlab_client.get('', function(_error, _resM, _body ){
		var user_collections = []
		for (var i = _body.length - 1; i >= 0; i--) {
			if(_body[i]== "system.indexes" ) { continue; }
			else{
				user_collections.push ( {"recurso":_body[i],"url":`/${API_VERSION}/`+_body[i] })
				console.log (" Recurso :: [ "+(user_collections.length)+" ] "+_body[i]+`\t\t url:/${API_VERSION}/`+_body[i]);
			}
		}
		console.log ("Bank-API :: Resources :: \n" +  user_collections );
		res.send( user_collections )
	})
})
/*
//____________________________________________USUARIOS
router.get('/usuarios', function(req, res){
	console.log("Bank-API :: /usuarios ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/usuarios?" + mlab_app_key );
	mlab_client.get('', function(_error, _resM, _body ){
		var usuarios = [];
		usuarios.push ( {"total_usuarios":_body.length} )
		usuarios.push ( {"data":_body} )
		res.send( usuarios )
	})
})

// usuarios id
router.get('/usuarios/:id', function(req, res){	
	console.log("Bank-API :: /usuarios  id:[" +req.params.id+ "]");
	
	req.checkParams("id", "El 'id' usuarios no es correcto").notEmpty().contains('USR');
	
	var errors = req.validationErrors();
	if (errors) {
	//	Status Code Definitions / 406 Not Acceptable  ## https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
		log.info("Bank-API :: /usuarios  id:[" +req.params.id+ "]");
		mlab_client	=  requestJson.createClient( mlab_base_path + "/usuarios" )
		var query  	= '?q={"idusuario":"'+req.params.id+'"}&' + mlab_app_key
		mlab_client.get( query , function(error, _resM , _body){
			res.send( _body )
		})
	}	
})

// usuario post
router.post('/usuarios', function(req, res){
// REQUERIDOS:    "nombre":"Gabie","apellido":"Jephcott","email":"gjephcott0@weebly.com","pais":"Vietnam","password":12345,
// "id":10000001, "idusuario":"USR10000001", "estado":"fueraLinea"
	var nuevo = req.body
//	console.log( "Data : ", nuevo)
	req.checkBody("nombre", "El 'nombre' no es correcto, favor de verificar.").isAlpha('es-ES');
	req.checkBody("apellido", "El 'apellido' no es correcto, favor de verificar.").isAlpha('es-ES');
	req.checkBody("pais", "El 'pais' no es correcto, favor de verificar.").exists();
	req.checkBody("password", "El 'password' no cumple con longitud minima de 6, favor de verificar.").exists().isLength({ min: 6 });
	req.checkBody("email", "El 'email' no es correcto, favor de verificar.").isEmail();
	
	var errors = req.validationErrors();
	if (errors) {
	//	Status Code Definitions / 406 Not Acceptable  ## https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
	// normal processing here
		var d = new Date();
		var d_iso_8601 = d.toISOString();  // ISO 8601 
		 
		console.log("Bank-API :: /usuarios Data : ", nuevo);
	//	Default values	 
		nuevo.estado  	=  "fueraLinea"
		nuevo.idusuario =  "USR10000001"
		nuevo.id  		=  "10000001"
		nuevo.accesos   =  {"registro":d_iso_8601 }		
	//	
		mlab_client		=  requestJson.createClient( mlab_base_path + "/usuarios?" + mlab_app_key )
		mlab_client.post( '' , req.body , function(error, _resM , _body){
			var new_user   = []
			    new_user.push( {'SUCCESS':_body});
			res.send( new_user )
		})	
	}// else
})

// usuarios 
router.put('/usuarios/:id', function(req, res){	
	console.log("Bank-API :: /usuarios  id:[" +req.params.id+ "]");
	
	req.checkParams("id", "El 'id' usuarios no es correcto").notEmpty().contains('USR');
	
	var errors = req.validationErrors();
	if (errors) {
	//	Status Code Definitions / 406 Not Acceptable  ## https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
		log.info("Bank-API :: /usuarios  id:[" +req.params.id+ "]");
		mlab_client		= requestJson.createClient( mlab_base_path + "/usuarios" )
		var query  		= '?q={"idusuario":'+req.params.id+'}&'
		var cambios 	= '&{ "$set":{'+stringify(req.body)+'}}'
		console.log ("cambios: Y	"  +cambios)
		mlab_client.put( query + mlab_app_key, function(error, _resM , cambios){
			res.send( cambios )
		})
	}
})



// ____________________________________________CUENTAS
router.get('/cuentas', function(req, res){
	console.log("Bank-API :: /cuentas ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
	var query = '?f={"movimientos":0}&' + mlab_app_key
	mlab_client.get( query, function(error, _resM , _body){
		var cuentas = [];		
		cuentas.push ( {"total_cuentas":_body.length} )
		cuentas.push ( {"data":_body} )
		res.send( cuentas )
	})
})

router.post('/cuentas/:id', function(req, res){	
	console.log("Bank-API :: /cuentas  id:[" +req.params.id+ "]");

	req.checkParams("id", "El 'id' cuentas no es correcto").notEmpty().contains('CTA');
	
	var errors = req.validationErrors();
	if (errors) {
	//	Status Code Definitions / 406 Not Acceptable  ## https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
		mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
		var query = '?q={"idcuenta":'+req.params.id+'}&f={"movimientos":0}&' + mlab_app_key
	//	console.log (query)
		mlab_client.get( query, function(error, _resM , _body){
			res.send( _body )
		})
	}
})

router.post('/cuentas/:id/movimientos', function(req, res){
	console.log("Bank-API :: /cuentas  id:[" +req.params.id+ "] /movimientos ");	

	var errors = req.validationErrors();
	if (errors) {
	//	Status Code Definitions / 406 Not Acceptable  ## https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
		res.status(406).send(errors);
		return;
	} else {
		mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
		var query = '?q={"idcuenta":'+req.params.id+'}&f={"movimientos":1}&' + mlab_app_key
	//	console.log (query)
		mlab_client.get( query, function(error, _resM , _body){
			res.send( _body )
		})
	}
})


router.put('/cuentas/:id', function(req, res){
	console.log("Bank-API :: /cuentas  id:[" +req.params.id+ "] ");
	mlab_client		= requestJson.createClient( mlab_base_path + "/cuentas" )
	var query  		= '?q={"idcuenta":'+req.params.id+'}&'
	var cambios 	= '&{ "$set":{'+stringify(req.body)+'}}'
//	console.log ("cambios: Y	"  +cambios)
	mlab_client.put( query + mlab_app_key, function(error, _resM , cambios){
		res.send( cambios )
	})
})
// ____________________________________________MOVIMIENTOS

router.get('/movimientos', function(req, res){
	console.log("Bank-API :: /movimientos ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/movimientos?" + mlab_app_key )
	mlab_client.get( '' , function(_error, _resM, _body ){
		var movimientos = [];		
		movimientos.push ( {"total_movimientos":_body.length} )
		movimientos.push ( {"data":_body} )
		res.send( movimientos )
	})
})

router.post('/movimientos', function(req, res){
	console.log("Bank-API :: /movimientos ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/movimientos?" + mlab_app_key )
	mlab_client.post( '' , req.body , function(error, _resM , _body){
		res.send( _body )
	})
})

router.post('/movimientos/:id', function(req, res){	
	console.log("Bank-API :: /movimientos  id:[" +req.params.id+ "] ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/movimientos" )
	var query = '?q={"idmovimiento":'+req.params.id+'}&'
//	console.log (query)
	mlab_client.get( query + mlab_app_key, function(error, _resM , _body){
		res.send( _body )
	})
})
router.post('/movimientos/cuentas/:id', function(req, res){	
	console.log("Bank-API :: /movimientos/cuentas/  id:[" +req.params.id+ "] ");
	mlab_client		=  requestJson.createClient( mlab_base_path + "/cuentas" )
	var query = '?q={"idcuenta":'+req.params.id+'}&f={"movimientos":1}&' + mlab_app_key
//	console.log (query)
	mlab_client.get( query, function(error, _resM , _body){
		res.send( _body )
	})
})

router.put('/movimientos/:id', function(req, res){	
	console.log("Bank-API :: /movimientos  id:[" +req.params.id+ "] ");
	mlab_client		= requestJson.createClient( mlab_base_path + "/movimientos" )
	var query  		= '?q={"idmovimiento":'+req.params.id+'}&'
	var cambios 	= '&{ "$set":{'+stringify(req.body)+'}}'
//	console.log ("cambios: Y	"  +cambios)
	mlab_client.put( query + mlab_app_key, function(error, _resM , cambios){
		res.send( cambios )
	})
})

*/

module.exports = router;