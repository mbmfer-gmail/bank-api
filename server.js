'use strict';

//    Constants
const PORT 			= 3000;
const HOST 			= '127.0.0.1';
const API_VERSION 	= 'v1.0';
const PROJECT_TITLE	= 'Tech University 2018';
const PROJECT_NAME	= 'Bank-API | APP';
const PROJECT_AUTOR	= 'Autor:  MB65379 | MFMB | FerHarris';


//    Package's & Moduls Dependencies		
const express 		= require('express');
const bodyParser 	= require('body-parser');
const jsonQuery 	= require('json-query');
const requestJson	= require('request-json');
const stringify 	= require('json-stringify');
const path 			= require('path')
const https 		= require('http')
//const log4js 		= require('log4js');
//const debug 		= require('debug')(`${PROJECT_TITLE}`);

const validator		= require('express-validator');

const routeIndex	= require('./routes/index');
const routeBank		= require('./routes/bank');
const routeCuenta	= require('./routes/cuenta');
const routeUsuario  = require('./routes/usuario');

//const route_usuario = require('./routes/usuario'); 
//const route_cuenta 	= require('./routes/cuenta');

//const { check, validationResult } 	= require('express-validator/check');
//const { matchedData, sanitize } 	= require('express-validator/filter');


/**
 * make a log directory, just in case it isn't there.
 */
/*
try {
  require('fs').mkdirSync('./log');
} catch (e) {
  if (e.code != 'EEXIST') {
    console.error("Could not set up log directory, error was: ", e);
    process.exit(1);
  }
}
*/
//    
const app 			= express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');




// replace this with the log4js connect-logger
// app.use(logger('dev'));
/**
 * Initialise log4js first, so we don't miss any log messages
 */
//log4js.configure('./config/log4js.json');
//const log 			= log4js.getLogger("app");
//app.use(log4js.connectLogger(log4js.getLogger("http"), { level: 'auto' }));
app.use(bodyParser.json());
//app.use(bodyParser.urlencoded());
// middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(validator());

app.use('/', routeIndex);
app.use(`/${API_VERSION}`, routeBank);
app.use(`/${API_VERSION}`, routeUsuario);
app.use(`/${API_VERSION}`, routeCuenta);


/// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});


/// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
    //  log.error("Something went wrong:", err);
        console.error("Something went wrong:", err);
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    //log.error("Something went wrong:", err);
    console.error("Something went wrong:", err);
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});




app.listen(PORT);
console.log('=========================================');     
console.log(` > ${PROJECT_TITLE}                      `);
console.log(` > ${PROJECT_NAME}                       `);
console.log(` > ${PROJECT_AUTOR}                      `);
console.log('=========================================');     
console.log('  ###            #         ##  ###  ###  ');
console.log('  #  #  ##  # #  #  #      ##  #  #  #   ');
console.log('  #  #    # ## # # #      #  # #  #  #   ');
console.log('  ###   ### #  # ##       #  # ###   #   ');
console.log('  #  # #  # #  # # #      #### #     #   ');
console.log('  #  # # ## #  # #  #     #  # #     #   ');
console.log('  ###   # # #  # #   #    #  # #    ###  ');
console.log('=========================================');
console.log(` >Running on http://${HOST}:${PORT}/${API_VERSION}/`);
console.log('========================================='); 


