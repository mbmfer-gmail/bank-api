# Project:  	bank-api

	Practitioner Bootcamp TechU   2018 Edition.

## Autor:			
	
	MB65379 | MFMB | FerHarris

### Description:
Bank API for BackEnd
   + Add :: **Users**  	> (SingUp / SingIn / SingOut)
   + Add & List :: **Accounts**
   + Add & List :: **Movement**


### Componentes Relacionados:
 + **bank-api**
 + bank-front
 + bank-presentation
