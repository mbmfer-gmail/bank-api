#Imagen Bank-API
FROM    node:slim
#Carpeta de la app
WORKDIR /bank-api
#Copia archivos
ADD . /bank-api
#Paquetes necesarios
RUN npm install
#Puerto que expongo
EXPOSE 3000
#Comando de inicio
CMD ["npm", "start"]