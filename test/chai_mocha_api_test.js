const chai 		= require('chai');		// motor de reglas
const chaiHttp 	= require('chai-http');
const mocha 	= require('mocha');
const should	= chai.should();
const server    = require('../server');
chai.use(chaiHttp);  // configura  chai con el modulo http

//    Constants
const PORT 			= 3000;
const HOST 			= '127.0.0.1';
const API_VERSION 	= 'v1.0';
const PROJECT_TITLE	= 'TechU 2018';
const PROJECT_NAME	= 'Bank-API';
const PROJECT_AUTOR	= 'Autor:  MB65379 | MFMB ';
const INFO = `Proyect: ${PROJECT_NAME} : ${API_VERSION}`
const url_base = `http://${HOST}:${PORT}`
const url_api  = `http://${HOST}:${PORT}/${API_VERSION}`
// ----------------------------------------------------------------[ Base Home ]
describe( INFO , () => {
	it('Exist Home ',(done) => {
		chai.request(url_base).get('/')
			.end((err, res) =>{
			//	console.log(res.body)
				res.should.have.status(200)
				done()
			})
	})
})

// ----------------------------------------------------------------[ USUARIOS ]
describe( INFO + ' Usuarios ', () => {

	it('Lista ',(done) => {
		chai.request(url_api).get('/usuarios')
			.end((err, res) =>{
				console.log(res.body)
				res.should.have.status(200)
			//	res.body.should.be.a('array')
			//	res.body.length.should.be.eql(1);
            //  res.body.should.have.property('total_usuarios').with.length(1);				
				done()
		})
	})

	it('Acceso ',(done) => {
		chai.request(url_api).post('/usuarios/acceso')
			.send({	"email":"rmalarkey2l@wisc.edu","pais":"Mexico","password":12345})
			.end((err, res) =>{
				console.log(res.body)
				res.should.have.status(200)
			//	res.body.should.be.a('array')
			//	res.body.length.should.be.eql(1);
            //  res.body.should.have.property('total_usuarios').with.length(1);				
				done()
		})
	})
	/*
	it('Filter x ID',(done) => {
		chai.request(url_api).post('/usuarios/USR00000921')
			.end((err, res) =>{
			console.log(res.body)
			res.should.have.status(200)
			res.body.should.be.a('array')
			res.body.length.should.be.eql(1) // user, account, 
			done()
		})
	})
*/
/*
	it('Usuarios / Creación ',(done) => {
	// "nombre":"Gabie","apellido":"Jephcott","email":"gjephcott0@weebly.com",
	// "pais":"Vietnam","password":12345
		chai.request(url_api).post('/usuarios')
			.send({	"nombre":"Fero","apellido":"Mazi","email":"fero-1@test.mx","pais":"Mexico","password":123456})
			.end((err, res) =>{
				console.log(res.body)
				res.should.have.status(200)
				res.body.should.be.a('array')
			//	res.body.length.should.be.eql(1) // user, account, 

				res.should.be.json;
				res.body.should.be.a('object');
				res.body.should.have.property('SUCCESS');
				res.body.SUCCESS.should.be.a('object');
				res.body.SUCCESS.should.have.property('_id');
				res.body.SUCCESS.should.have.property('idusuario');
				res.body.SUCCESS.should.have.property('name');
				res.body.SUCCESS.should.have.property('lastName');
				res.body.SUCCESS.should.have.property('email');
				res.body.SUCCESS.should.have.property('estado');								
				done();
		})		
	})
*/

})


// ----------------------------------------------------------------
/*	*/
describe('Test de API   Cuentas ', () => {

	it('API / Cuentas / Lista Completa ',(done) => {
		chai.request(url_api).get('/cuentas')
			.end((err, res) =>{
				console.log(res.body)
				res.should.have.status(200);				
				done();
		})
	})

	//		id: 993 , "idcuenta" : "CTA00000993" , "idusuario" : "USR00000749"
	it('API / Cuentas / Filter ID ',(done) => {
		chai.request(url_api).post('/movimientos/cuentas/993')
		//	.send({"id":993})		
			.end((err, res) =>{
				console.log(res.body)
				res.should.have.status(200);				
				done();
		})
	})

})
/*	*/
// ----------------------------------------------------------------
/*
describe('Test de API v3  Movimientos ', () => {

	it('API / Movimientos / Lista Completa',(done) => {
		chai.request('http://localhost:3000').get('/v3/movimientos')
			.end((err, res) =>{
				//console.log(res)
			res.should.have.status(200)
			res.body.should.be.a('array')
			done()
		})
	})

})
*/